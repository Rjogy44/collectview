//
//  ViewController2.swift
//  SevenWonders
//
//  Created by Randy Jorgensen on 2/22/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//


import UIKit


class VIewController2ViewController: UIViewController {
    
    var jsonResult1: Array<AnyObject>!
    var rowOfStuff = Singleton.sharedInstance().rowNumber
    var wArray: [String] = []
    var wName: String = ""
    var wImage: String = ""
    var wLocation: String = ""
    var wRegion: String = ""
    var wYearBuilt: String = ""
    var wLong: String = ""
    var wLat: String = ""
    var wWiki: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let data = NSData(contentsOfURL: NSURL(string: "http://aasquaredapps.com/Class/sevenwonders.json")!)
        do {
        if let jsonResult: AnyObject = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) {
            
            if let outerLevel = jsonResult as? NSArray{
                if let innerLevel = outerLevel[rowOfStuff] as? NSDictionary{
                    wName = innerLevel.objectForKey("name") as! String
                    wImage = innerLevel.objectForKey("image") as! String
                    wLocation = innerLevel.objectForKey("location")! as! String
                    wRegion = innerLevel.objectForKey("region")! as! String
                    wYearBuilt = innerLevel.objectForKey("year_built")! as! String
                    let wLong1:NSNumber = innerLevel.objectForKey("longitude") as! NSNumber
                    wLong = wLong1.stringValue
                    let wLat1:NSNumber = innerLevel.objectForKey("latitude")! as! NSNumber
                    wWiki = innerLevel.objectForKey("wikipedia")! as! String
                    wLat = wLat1.stringValue
                    wArray.append(wLocation)
                    wArray.append(wRegion)
                    wArray.append(wYearBuilt)
                    wArray.append(wLong)
                    wArray.append(wLat)
                    wArray.append(wWiki)
                    
                            //Finally We Got The Name
                            
                    
                }
            }
            
            
        }
    } catch let error as NSError {
    print("Error:\n \(error)")
    return
    }
}

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var location: UILabel!

    @IBOutlet weak var region: UILabel!
    
    @IBOutlet weak var year: UILabel!
    
    @IBOutlet weak var long: UILabel!
    
    @IBOutlet weak var lat: UILabel!
    
    @IBOutlet weak var wiki: UILabel!

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        self.name.text = wName
        let url1 = wImage
        self.location.text = wLocation
        self.region.text = wRegion
        self.year.text = wYearBuilt
        self.long.text = wLong
        self.lat.text = wLat
        self.wiki.text = wWiki
        
        
        
        if let url = NSURL(string: url1) {
            if let data = NSData(contentsOfURL: url) {
                image.image = UIImage(data: data)
            }        
        }
        
    }
    
    
        
    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
}
