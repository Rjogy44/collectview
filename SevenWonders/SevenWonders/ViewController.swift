//
//  ViewController.swift
//  SevenWonders
//
//  Created by Randy Jorgensen on 2/21/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var nameArray: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let data = NSData(contentsOfURL: NSURL(string: "http://aasquaredapps.com/Class/sevenwonders.json")!)
        do {
            if let jsonResult: Array<AnyObject>! = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? Array {
                
                for wonderName in jsonResult! {
                    let wName = wonderName["name"]! as! String
                    nameArray.append(wName)
                }
                

            }
        } catch let error as NSError {
            print("Error:\n \(error)")
            return
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return nameArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        
        cell.textLabel?.text = nameArray[indexPath.row]
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        Singleton.sharedInstance().rowNumber = indexPath.row
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            
            
            
            
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

