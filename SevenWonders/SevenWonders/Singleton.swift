//
//  Singleton.swift
//  SevenWonders
//
//  Created by Randy Jorgensen on 2/22/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import Foundation

class Singleton {
    static var instance: Singleton!
    var rowNumber = Int()
    
    // SHARED INSTANCE
    class func sharedInstance() -> Singleton {
        self.instance = (self.instance ?? Singleton())
        return self.instance
    }
}