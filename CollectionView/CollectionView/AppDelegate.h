//
//  AppDelegate.h
//  CollectionView
//
//  Created by Randy Jorgensen on 2/29/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) NSMutableArray *myArray;


@end

