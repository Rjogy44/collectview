//
//  DetailViewController.m
//  CollectionView
//
//  Created by Randy Jorgensen on 2/29/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
#import "DetailViewController.h"


@interface DetailViewController()

@end

@interface NSArray(JSONCategories)
+(NSArray*)dictionaryWithContentsOfJSONURLString:
(NSString*)urlAddress;
-(NSData*)toJSON;
@end

@implementation NSArray(JSONCategories)
+(NSArray*)dictionaryWithContentsOfJSONURLString:
(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL:
                    [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self
                                                options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;    
}
@end@implementation DetailViewController
typedef void(^myCompletion)(NSArray* data);

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor greenColor];
    [self.view addSubview:sv];
    
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(125, 100, 125, 125)];
    [sv addSubview:iv];

    
    UILabel* label1 = [[UILabel alloc]initWithFrame:CGRectZero];
    label1.contentMode = UIViewContentModeScaleAspectFit;
    [label1 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label1.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label1];
    
    UILabel* label2 = [[UILabel alloc]initWithFrame:CGRectZero];
    label2.contentMode = UIViewContentModeScaleAspectFit;
    [label2 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label2.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label2];
    
    UILabel* label3 = [[UILabel alloc]initWithFrame:CGRectZero];
    label3.contentMode = UIViewContentModeScaleAspectFit;
    [label3 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label3.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label3];
    
    UILabel* label4 = [[UILabel alloc]initWithFrame:CGRectZero];
    label4.contentMode = UIViewContentModeScaleAspectFit;
    [label4 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label4.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label4];
    
    UILabel* label5 = [[UILabel alloc]initWithFrame:CGRectZero];
    label5.contentMode = UIViewContentModeScaleAspectFit;
    [label5 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label5.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label5];
    
    UILabel* label6 = [[UILabel alloc]initWithFrame:CGRectZero];
    label6.contentMode = UIViewContentModeScaleAspectFit;
    [label6 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label6.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label6];
    
    UILabel* label7 = [[UILabel alloc]initWithFrame:CGRectZero];
    label7.contentMode = UIViewContentModeScaleAspectFit;
    [label7 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label7.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label7];
    
    UILabel* label17 = [[UILabel alloc]initWithFrame:CGRectZero];
    label17.contentMode = UIViewContentModeScaleAspectFit;
    label17.text = @"Name";
//    [label17 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label17.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label17];
    
    UILabel* label8 = [[UILabel alloc]initWithFrame:CGRectZero];
    label8.contentMode = UIViewContentModeScaleAspectFit;
    label8.text = @"Location";
//    [label8 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label8.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label8];
    
    UILabel* label9 = [[UILabel alloc]initWithFrame:CGRectZero];
    label9.contentMode = UIViewContentModeScaleAspectFit;
    label9.text = @"Region";
//    [label9 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label9.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label9];
    
    UILabel* label10 = [[UILabel alloc]initWithFrame:CGRectZero];
    label10.contentMode = UIViewContentModeScaleAspectFit;
    label10.text = @"Year Built";
//    [label10 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label10.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label10];
    
    UILabel* label11 = [[UILabel alloc]initWithFrame:CGRectZero];
    label11.contentMode = UIViewContentModeScaleAspectFit;
    label11.text = @"Wikipedia";
//    [label11 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label11.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label11];
    
    UILabel* label12 = [[UILabel alloc]initWithFrame:CGRectZero];
    label12.contentMode = UIViewContentModeScaleAspectFit;
    label12.text = @"Latitude";
//    [label12 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label12.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label12];
    
    UILabel* label13 = [[UILabel alloc]initWithFrame:CGRectZero];
    label13.contentMode = UIViewContentModeScaleAspectFit;
    label13.text = @"Longitude";
//    [label13 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    label13.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label13];
    
    
 

    


    NSInteger int1 = [_id1 integerValue];
    arrayOfSevenWonders = [NSMutableArray new];
    
    NSArray* dict1 =
    [NSArray dictionaryWithContentsOfJSONURLString:
     @"http://aasquaredapps.com/Class/sevenwonders.json"];
        
        NSDictionary *dict = [dict1 objectAtIndex:int1];
    
            NSString *ns = [dict valueForKey:@"image"];
            NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:ns]];
            iv.image  =  [UIImage imageWithData: imageData];
            label2.text =  [dict valueForKey:@"location"];
            label1.text = [dict valueForKey:@"name"];
            label3.text = [dict valueForKey:@"region"];
            label5.text = [dict valueForKey:@"wikipedia"];
            label4.text = [dict valueForKey:@"year_built"];
            NSNumber *lat = [dict valueForKey:@"latitude"];
            NSNumber *lon = [dict valueForKey:@"longitude"];
            NSString *lat1 = [lat stringValue];
            NSString *lon1 = [lon stringValue];
            label6.text = lat1;
            label7.text = lon1;

    
    
    NSDictionary* variableDictionary = NSDictionaryOfVariableBindings(sv,label1,label2,label3,label4,label5,label6,label17,label8,label9,label10,label11,label12,label13,label7);
    
    NSDictionary* metrics = nil;
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label1]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label2]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label3]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label4]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label5]-|" options:0 metrics:metrics views:variableDictionary]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-300-[label17]-[label1]-[label8]-[label2]-[label9]-[label3]-[label10]-[label4]-[label11]-[label5]-[label12]-[label6]-[label13]-[label7]|" options:0 metrics:metrics views:variableDictionary]];

    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end